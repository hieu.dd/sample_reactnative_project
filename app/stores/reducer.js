import { combineReducers } from 'redux';

import appState from './appState/reducer';
import firebase from './firebase/reducer';

// Combines all reducers to a single reducer function
const reducers = combineReducers({
  appState,
  firebase,
});

export default reducers;
