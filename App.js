import React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text, Image } from 'react-native';
import { Provider } from 'react-redux';
import { AppLoading, Font } from 'expo';

import firebase from 'firebase';
import JsSaleLib from 'teko-js-sale-library';

import store, { persistor } from './app/stores/configureStore';
import AppNavigator from './app/navigators/AppNavigator';
// import TRACK from './app/config/TrackingPrototype';
import config from './app/config';
import RnExpoPlatform from './app/utils/rn-expo-platform';
import { scale } from './app/utils/scaling';
import { PersistGate } from 'redux-persist/es/integration/react';

import { useScreens } from 'react-native-screens';
firebase.initializeApp(config.firebase);
JsSaleLib.init(RnExpoPlatform, config);
// JsSaleLib.Tracking.init(TRACK.PROTOTYPE);
JsSaleLib.Tracking.addAutoParams({ version: config.version });
JsSaleLib.Util.Api.addCredential('default', { username: 'offlinesales-vs', password: 'voucherservice@Oct', type: 'Basic' });
JsSaleLib.Service.Voucher.setAuthorization('default');

export default class App extends React.Component {
  state = {
    animating: true,
    isLoadingComplete: false,
    updateMessage: 'Đang kiểm tra phiên bản mới...',
    updateExpoAvailable: false,
    updateAndroidAvailable: false,
    updateIosAvailable: false,
    updating: false,
    updateFinished: false,
    noUpdateAvailable: false,
  };

  _loadResourcesAsync = async () => {
    return Promise.all([
      Font.loadAsync({
        saletool: require('./app/resources/fonts/saletool.ttf'),
        'sale-text-italic': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-Italic.ttf'),
          android: require('./app/resources/fonts/android/Roboto-Italic.ttf'),
        }),
        'sale-text-light': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-Light.ttf'),
          android: require('./app/resources/fonts/android/Roboto-Light.ttf'),
        }),
        'sale-text-light-italic': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-LightItalic.ttf'),
          android: require('./app/resources/fonts/android/Roboto-LightItalic.ttf'),
        }),
        'sale-text-medium': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-Medium.ttf'),
          android: require('./app/resources/fonts/android/Roboto-Medium.ttf'),
        }),
        'sale-text-regular': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-Regular.ttf'),
          android: require('./app/resources/fonts/android/Roboto-Regular.ttf'),
        }),
        'sale-text-bold': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-Bold.ttf'),
          android: require('./app/resources/fonts/android/Roboto-Bold.ttf'),
        }),
        'sale-text-semibold': Platform.select({
          ios: require('./app/resources/fonts/ios/SF-UI-Text-Semibold.ttf'),
          android: require('./app/resources/fonts/android/Roboto-Medium.ttf'),
        }),
      }),
    ]);
  };

  _handleLoadingError = error => {
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <View style={{ flex: 1 }}>
          <AppLoading
            startAsync={this._loadResourcesAsync}
            onError={this._handleLoadingError}
            onFinish={this._handleFinishLoading}
          />
        </View>
      );
    } else {
      return (
        <Provider store={store}>
          <PersistGate persistor={persistor} loading={<View style={{ backgroundColor: 'green' }} />}>
            <View style={styles.container}>
              {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
              <AppNavigator />
              {/* <ProcessingPrompt />
          <AnimationAlertModal /> */}
            </View>
          </PersistGate>
        </Provider>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
