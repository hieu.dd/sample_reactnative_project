import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions, Alert } from 'react-native';
import { connect } from 'react-redux';

export class HomeContainer extends Component {
  render() {
    return <View style={{ backgroundColor: 'red', flex: 1 }} />;
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);

const styles = StyleSheet.create({
  inputContainer: {
    alignItems: 'center',
    padding: 15,
    flexDirection: 'row',
  },
  headerContainer: {
    backgroundColor: '#1c65aa',
    paddingTop: 10,
  },
});
