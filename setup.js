var Cli = require('teko-js-sale-cli');

Cli.setupAll(
  {
    // common config
    store_version: '1.0.1',
    sprint: '09',
    build_number: '03',
    extra: require('./variables'),
    data: {
      largeFileAsAsset: true,
      target: ['product_specification'],
    },
    directories: {
      data_folder: `${__dirname}/app/resources/data`,
      config_folder: `${__dirname}/app/config`,
      root_folder: `${__dirname}`,
      tracking_doc: `${__dirname}/tracking_document.xlsx`,
    },
  },
  {
    // release mail config
    app_name: 'ĐẠI LÝ PHONG VŨ',
    expo_project_path: '@tiendv/phong-vu-shopping',
    jira_board: 'DLPV',
    general: {
      app_description: 'Đại lý Phong Vũ (mobile app)',
      git_url: 'https://git.teko.vn/oSales/dai-ly-phong-vu.git',
      document_url: 'https://confluence.teko.vn/display/TekBack/3.1.1+Screen+mockups',
      design_url: 'https://projects.invisionapp.com/d/main#/projects/prototypes/15659558',
    },
    theme: {
      main_color: '#950217',
      secondary_color: '#ea341f',
    },
    bug_list: ['433'],
    feature_list: ['427', '412', '415', '437'],
    note_list: ['Không có'],
    receivers: [
      `'tien.dv@teko.vn'`,
      `'ngan.dt@teko.vn'`,
      `'trang.nt1@teko.vn'`,
      `'thanh.tt@teko.vn'`,
      `'an.ct@teko.vn'`,
      `'xuan.nt@teko.vn'`,
      `'thanh.bm@teko.vn'`,
      `'hieu.dd@teko.vn'`,
      `'huy.lv@teko.vn'`,
      `'tao.nd@teko.vn'`,
      `'tu.nb@teko.vn'`,
    ],
  }
);
