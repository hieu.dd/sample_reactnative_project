import firebase from 'firebase';
import PromoManager from 'teko-promotion-parser';

import * as types from './action-types';
// import { checkCartPromotionAndChangeIfInvalid } from '../cart/actions';

let RESOLVER = {
  promotions_old: null,
  promotions_new: null,
};

let TASK = {
  ...TASK,
  promotions: {
    firstLoad: true,
    promise: new Promise((resolve, reject) => {
      RESOLVER['promotions_old'] = resolve;
    }),
  },
  promotions_new: {
    firstLoad: true,
    promise: new Promise((resolve, reject) => {
      RESOLVER['promotions_new'] = resolve;
    }),
  },
};

let LISTENING_PROMOTIONS_LINK = [];

const resolveTask = taskName => {
  if (TASK[taskName].firstLoad) {
    TASK[taskName].firstLoad = false;
    if (RESOLVER[taskName]) {
      RESOLVER[taskName](true);
    }
  }
};

export function resetPromotionsTask() {
  RESOLVER = {
    ...RESOLVER,
    promotions_old: null,
    promotions_new: null,
  };
  TASK = {
    ...TASK,
    promotions_old: {
      firstLoad: true,
      promise: new Promise((resolve, reject) => {
        RESOLVER['promotions_old'] = resolve;
      }),
    },
    promotions_new: {
      firstLoad: true,
      promise: new Promise((resolve, reject) => {
        RESOLVER['promotions_new'] = resolve;
      }),
    },
  };
}

export const listenFirebaseData = (path, localPath, isOff, callback) => {
  return (dispatch, getState) => {
    try {
      let ref = firebase.database().ref(path);
      ref.off('value');
      ref.on('value', snapshot => {
        let data = snapshot.val() ? snapshot.val() : {};
        dispatch({
          type: types.SYNC_FIREBASE_DATA_RESULT,
          path: localPath,
          data,
        });
        if (isOff) LISTENING_PROMOTIONS_LINK.push(path);
        // dispatch({
        //   type: types.SYNC_FIREBASE_LINK_PROMOTION_ON,
        //   link: path
        // });
        callback && callback(data);
      });
    } catch (error) {}
  };
};

export const getFirebaseData = (path, localPath, callback) => {
  return (dispatch, getState) => {
    try {
      firebase
        .database()
        .ref(path)
        .once('value', snapshot => {
          let data = snapshot.val() ? snapshot.val() : {};
          dispatch({
            type: types.SYNC_FIREBASE_DATA_RESULT,
            path: localPath,
            data,
          });
          callback && callback(data);
        });
    } catch (error) {}
  };
};

export const getFirebasePromotionsData = (path, localPath, callback) => {
  return (dispatch, getState) => {
    try {
      firebase
        .database()
        .ref(`simplified/${path}`)
        .once('value', snapshot => {
          let data = snapshot.val() ? snapshot.val() : {};

          // clear the current data first before listening new data
          dispatch({
            type: types.SYNC_FIREBASE_DATA_RESULT,
            path: localPath,
            data: {},
          });

          listenFirebasePromotion(dispatch, getState, path, localPath, data);
          callback && callback(data);
        });
    } catch (error) {}
  };
};

export const listenFirebasePromotion = (dispatch, getState, path, localPath, data) => {
  let waitingPromises = [];

  for (let key of Object.keys(data)) {
    if (key && data[key]) {
      let channels = [];
      let endDate = 0;

      if (path.indexOf('promotions_other') !== -1) {
        if (path.indexOf('student') !== -1) {
          endDate = data[key].data && data[key].data.endAt;
          channels = data[key].data && data[key].data.channels;
        } else {
          endDate = data[key].endAt;
          channels = data[key].channels;
        }
      } else if (path.indexOf('promotions_new') !== -1) {
        endDate = data[key].dateTime && data[key].dateTime.date && data[key].dateTime.date.end;
        channels = data[key].channels;
      } else {
        endDate = data[key].date && data[key].date.endDate;
        channels = data[key].channels;
      }

      if (channels && channels.findIndex(item => item === 'agent') !== -1 && endDate > Date.now()) {
        if (path.indexOf('promotions_other') !== -1) {
          dispatch(listenFirebaseData(`${path}/${key}`, `${localPath}/${key}`, true));
        } else if (path === 'promotions-pv/promotions_new') {
          waitingPromises.push(
            new Promise(resolve => {
              dispatch(
                listenFirebaseData(`${path}/${key}`, `${localPath}/${key}`, true, data => {
                  if (TASK['promotions_new'].firstLoad) {
                    resolve(data);
                  } else {
                    PromoManager.updateObject(getState().firebase.promotionsNew);
                    // dispatch(checkCartPromotionAndChangeIfInvalid());
                  }
                })
              );
            })
          );
        } else {
          waitingPromises.push(
            new Promise(resolve => {
              dispatch(
                listenFirebaseData(`${path}/${key}`, `${localPath}/${key}`, true, data => {
                  if (TASK['promotions_old'].firstLoad) {
                    resolve(data);
                  } else {
                    // dispatch(checkCartPromotionAndChangeIfInvalid());
                  }
                })
              );
            })
          );
        }
      }
    }
  }

  if (path === 'promotions-pv/promotions_new') {
    Promise.all(waitingPromises).then(values => {
      resolveTask('promotions_new');
      dispatch(setInitializePromotionsNewDone());
      PromoManager.updateObject(getState().firebase.promotionsNew);
    });
  } else if (path === 'promotions-pv/promotions') {
    Promise.all(waitingPromises).then(values => {
      dispatch(setInitializePromotionsOldDone());
      resolveTask('promotions_old');
    });
  }
};

export const offLinkFirebase = () => {
  LISTENING_PROMOTIONS_LINK.forEach(link => {
    let ref = firebase.database().ref(link);
    ref.off('value');
  });
  LISTENING_PROMOTIONS_LINK = [];
  // dispatch({
  //   type: types.SYNC_FIREBASE_LINK_PROMOTION_ON,
  //   reset: true
  // });
};

export const listenPromotions = () => {
  return (dispatch, getState) => {
    resetPromotionsTask();
    offLinkFirebase();

    dispatch(getFirebasePromotionsData('promotions-pv/promotions_other/voucher', 'vouchers'));
    // dispatch(getFirebasePromotionsData('promotions-pv/promotions_other/student', 'promotionsOther/student'));
    dispatch(getFirebasePromotionsData('promotions-pv/promotions_new', 'promotionsNew'));
    dispatch(getFirebasePromotionsData('promotions-pv/promotions', 'promotionsOld'));
    dispatch(validateCartPromotion());
  };
};

export const listenFirebaseConfig = () => {
  return (dispatch, getState) => {
    dispatch(listenFirebaseData('config', `config`));
  };
};

export const validateCartPromotion = () => {
  return (dispatch, getState) => {
    Promise.all([TASK.promotions_old.promise, TASK.promotions_new.promise]).then(values => {
      // dispatch(checkCartPromotionAndChangeIfInvalid());
    });
  };
};

export const setInitializePromotionsNewDone = () => {
  return {
    type: types.SET_INITIALIZE_PROMOTIONS_NEW_DONE,
  };
};

export const setInitializePromotionsOldDone = () => {
  return {
    type: types.SET_INITIALIZE_PROMOTIONS_OLD_DONE,
  };
};
