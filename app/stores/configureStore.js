import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import { DeviceEventEmitter } from 'react-native';

import thunk from 'redux-thunk';
import reducers from './reducer';
import config from '../config';
import { persistCombineReducers, persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/es/storage';

//  Returns the store instance
// It can  also take initialState argument when provided

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['appState'],
};
const rootReduces = persistReducer(persistConfig, reducers);

const configureStore = () => {
  if (config.log_enabled) {
    return {
      ...createStore(rootReduces, applyMiddleware(thunk, logger)),
    };
  } else {
    return {
      ...createStore(rootReduces, applyMiddleware(thunk)),
    };
  }
};
const store = configureStore();
export const persistor = persistStore(store, {}, () => DeviceEventEmitter.emit('PERSIST'));
export default store;
