import { NavigationActions } from 'react-navigation';

// reference: https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
class NavigationService {
  navigator = null;

  setTopLevelNavigator(navigatorRef) {
    this.navigator = navigatorRef;
  }

  navigate(route, params) {
    let routeName = null;
    let key = null;
    if (typeof route === 'string') {
      routeName = route;
    } else {
      ({ routeName, key } = route);
    }

    let action = { params };
    if (routeName) {
      action.routeName = routeName;
    }
    if (key) {
      action.key = key;
    }

    this.navigator && this.navigator.dispatch(NavigationActions.navigate(action));
  }

  dispatch(action) {
    this.navigator && this.navigator.dispatch(action);
  }

  goBack() {
    this.navigator && this.navigator.dispatch(NavigationActions.back());
  }
}

export function createNavigationService() {
  return new NavigationService();
}
