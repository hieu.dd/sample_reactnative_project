module.exports = {
  env: "production",
  version: "1.0.1.09403",
  store_version: "1.0.1",
  log_enabled: true,
  app_id: "dailymoi",
  firebase: {
    apiKey: "AIzaSyBKd_0GkWQSmGDjKfnS44PGTuVXWmbIbjw",
    authDomain: "live-phong-vu-shopping.firebaseapp.com",
    databaseURL: "https://live-phong-vu-shopping.firebaseio.com",
    projectId: "live-phong-vu-shopping",
    storageBucket: "live-phong-vu-shopping.appspot.com",
    messagingSenderId: "436624642744"
  },
  google_client_id: {
    iosClientId: "436624642744-t490ufknv5c1ipuqt7i8pn0731mkofhg.apps.googleusercontent.com",
    androidClientId: "436624642744-hr3q5ir5rodd03717pd0vdpse5mpb4ke.apps.googleusercontent.com",
    iosStandaloneAppClientId: "436624642744-umsars6cd4tht543qu2frcu37pshh04s.apps.googleusercontent.com",
    androidStandaloneAppClientId: "436624642744-4s3hq2eotgct0qo104kqsosmpd1n7rjd.apps.googleusercontent.com",
    webClientId: "436624642744-ncvjsgcj50nm4d22u62v93dq1itd6a3v.apps.googleusercontent.com"
  },
  hosts: {
    sso: "acc.teko.vn",
    offlinesales: "offlinesales.teko.vn",
    magento: "phongvu.vn",
    asia: "pvis.teko.vn",
    crm: "crm.phongvu.vn",
    ppm: "price.teko.vn",
    notification: "stn.teksvr.com",
    om: "om.teko.vn",
    product_search: "search.phongvu.vn"
  },
  PLAN_KEY: "P5D2DCZ173DU8K51TC0FA43A71814",
  AMPLITUDE_API_KEY: "6d010e2a5a8ff734ebcc00a475e20eb6",
  anonymous_access_token: "ost@ad7d4fb29d2e451182bd42d0e70ffca7",
  use_mock_data: true
}