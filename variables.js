module.exports = {
  dev: {
    app_id: 'dailymoi',
    firebase: {
      apiKey: 'AIzaSyACU3HUbr2KKwJIMWD7e_7atY_IpoVxCQ0',
      authDomain: 'phong-vu-dev.firebaseapp.com',
      databaseURL: 'https://phong-vu-dev.firebaseio.com',
      projectId: 'phong-vu-dev',
      storageBucket: 'phong-vu-dev.appspot.com',
      messagingSenderId: '967244876979'
    },
    google_client_id: {
      iosClientId: '967244876979-m2kufmpbup090evac3nh4272c7dti5d7.apps.googleusercontent.com',
      androidClientId: '967244876979-hk7tc4v8vd0rf0c51rjl1636459kn40k.apps.googleusercontent.com',
      webClientId: '967244876979-9r6areuva29go9dqpm8us8mk1f79e9kk.apps.googleusercontent.com'
    },
    hosts: {
      sso: 'acc.teko.vn',
      offlinesales: 'test.offlinesales.teksvr.com',
      magento: 'dev.tekshop.vn',
      asia: 'dev-pvis.teko.vn',
      crm: 'dev-crm.teko.vn',
      ppm: 'price-dev.teko.vn',
      notification: 'test.stn.teksvr.com',
      om: 'om-dev.teko.vn',
      product_search: 'search-dev.phongvu.vn'
    },
    PLAN_KEY: 'CENWLp0WlJxCW7xRuc3DWJ0ySPEHiAJv',
    AMPLITUDE_API_KEY: 'null',
    anonymous_access_token: 'ost@ad7d4fb29d2e451182bd42d0e70ffca7',
    use_mock_data: true
  },
  qc: {
    app_id: 'dailymoi',
    firebase: {
      apiKey: 'AIzaSyA-NMHeZIgArNEwCwdR9XOvTPxJijYXYHI',
      authDomain: 'phong-vu-qc.firebaseapp.com',
      databaseURL: 'https://phong-vu-qc.firebaseio.com',
      projectId: 'phong-vu-qc',
      storageBucket: 'phong-vu-qc.appspot.com',
      messagingSenderId: '507328081131'
    },
    google_client_id: {
      iosClientId: '507328081131-0rgt0m8ks90g0d6lrfhs02dvgoip01pm.apps.googleusercontent.com',
      androidClientId: '507328081131-90jto8bfcelvgrgl0vdp9a8bsdekliqi.apps.googleusercontent.com',
      webClientId: '507328081131-tief2osj8p3gv3qr4micbtpstpffdfe9.apps.googleusercontent.com'
    },
    hosts: {
      sso: 'acc.teko.vn',
      offlinesales: 'qc.offlinesales.teksvr.com',
      magento: 'test.tekshop.vn',
      asia: 'test-pvis.teko.vn', // 'test-pvis.teko.vn',
      crm: 'test-crm.teko.vn',
      ppm: 'price-test.teko.vn',
      notification: 'test.stn.teksvr.com',
      om: 'om-test.teko.vn',
      product_search: 'search-dev.phongvu.vn'
    },
    PLAN_KEY: 'TqJd0LMUbUPwQwAuerJgKIBesGrf91D5',
    AMPLITUDE_API_KEY: '5c264c3a0dc78d40b7fc92dc0da75e4b',
    anonymous_access_token: 'ost@ad7d4fb29d2e451182bd42d0e70ffca7',
    use_mock_data: true
  },
  staging: {
    app_id: 'dailymoi',
    firebase: {
      apiKey: 'TO-BE-GENERATED',
      authDomain: 'TO-BE-GENERATED',
      databaseURL: 'TO-BE-GENERATED',
      projectId: 'TO-BE-GENERATED',
      storageBucket: 'TO-BE-GENERATED',
      messagingSenderId: 'TO-BE-GENERATED'
    },
    google_client_id: {
      iosClientId: 'TO-BE-GENERATED',
      androidClientId: 'TO-BE-GENERATED',
      webClientId: 'TO-BE-GENERATED'
    },
    hosts: {
      sso: 'acc.teko.vn',
      offlinesales: 'staging.offlinesales.teksvr.com',
      magento: 'stg.tekshop.vn',
      asia: 'stag-pvis.teko.vn',
      crm: 'stg-crm.teko.vn',
      ppm: 'price-stg.teko.vn',
      notification: 'test.stn.teksvr.com',
      om: 'om-staging.teko.vn',
      product_search: 'search-dev.phongvu.vn'
    },
    PLAN_KEY: 'TqJd0LMUbUPwQwAuerJgKIBesGrf91D5',
    AMPLITUDE_API_KEY: '99743c920228b63d54a78b210a67664c',
    anonymous_access_token: 'c78c28ecf1b44b519dbc13feaf9d10ef',
    use_mock_data: true
  },
  production: {
    app_id: 'dailymoi',
    firebase: {
      apiKey: 'AIzaSyBKd_0GkWQSmGDjKfnS44PGTuVXWmbIbjw',
      authDomain: 'live-phong-vu-shopping.firebaseapp.com',
      databaseURL: 'https://live-phong-vu-shopping.firebaseio.com',
      projectId: 'live-phong-vu-shopping',
      storageBucket: 'live-phong-vu-shopping.appspot.com',
      messagingSenderId: '436624642744'
    },
    google_client_id: {
      iosClientId: '436624642744-t490ufknv5c1ipuqt7i8pn0731mkofhg.apps.googleusercontent.com',
      androidClientId: '436624642744-hr3q5ir5rodd03717pd0vdpse5mpb4ke.apps.googleusercontent.com',
      iosStandaloneAppClientId: '436624642744-umsars6cd4tht543qu2frcu37pshh04s.apps.googleusercontent.com',
      androidStandaloneAppClientId: '436624642744-4s3hq2eotgct0qo104kqsosmpd1n7rjd.apps.googleusercontent.com',
      webClientId: '436624642744-ncvjsgcj50nm4d22u62v93dq1itd6a3v.apps.googleusercontent.com'
    },
    hosts: {
      sso: 'acc.teko.vn',
      offlinesales: 'offlinesales.teko.vn',
      magento: 'phongvu.vn',
      asia: 'pvis.teko.vn',
      crm: 'crm.phongvu.vn',
      ppm: 'price.teko.vn',
      notification: 'stn.teksvr.com',
      om: 'om.teko.vn',
      product_search: 'search.phongvu.vn'
    },
    PLAN_KEY: 'P5D2DCZ173DU8K51TC0FA43A71814',
    AMPLITUDE_API_KEY: '6d010e2a5a8ff734ebcc00a475e20eb6',
    anonymous_access_token: 'ost@ad7d4fb29d2e451182bd42d0e70ffca7',
    use_mock_data: true
  }
};
