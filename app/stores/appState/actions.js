import firebase from 'firebase';
import { Util, Data } from 'teko-js-sale-library';

import * as types from './action-types';
// import * as utils from '../utils';

export function changeConnectionStatus(status) {
  return {
    type: types.CHANGE_CONNECTION_STATUS,
    isConnected: status,
  };
}

export function changeAppState(state) {
  return {
    type: types.CHANGE_APP_STATE,
    state,
  };
}

export function showLoadingModal({ title, loadingText, message, detail, backgroundColor, actions }) {
  return {
    type: types.SHOW_HIDE_LOADING_MODAL,
    title,
    loadingText,
    message,
    detail,
    actions,
    backgroundColor,
    visible: true,
  };
}

export function hideLoadingModal() {
  return {
    type: types.SHOW_HIDE_LOADING_MODAL,
    visible: false,
  };
}

export function showHideLoadingModal({ title, loadingText, message, visible, backgroundColor, actions }) {
  return {
    type: types.SHOW_HIDE_LOADING_MODAL,
    title,
    loadingText,
    message,
    visible,
    actions,
    backgroundColor,
  };
}

export function hideAnimationModal() {
  return {
    type: types.SHOW_HIDE_ANIMATION_MODAL,
    visible: false,
  };
}

export function showAnimationModal({ message, animationType, actions, title, detail }) {
  return {
    type: types.SHOW_HIDE_ANIMATION_MODAL,
    visible: true,
    message,
    detail,
    animationType,
    actions,
    title,
  };
}

export const initData = () => {
  return async (dispatch, getState) => {
    try {
      // let geodata = await utils.readJsonAssetFile(require('../../resources/data/geodata.tkon'));
      // geodata = Data.Optimizer.expandGeoData(geodata);
      // Util.GeoLoc.init(geodata);
    } catch (error) {}
  };
};

export function syncFirebaseConfig() {
  return async (dispatch, getState) => {
    try {
      let configSnapshot = await firebase
        .database()
        .ref('config')
        .once('value');

      dispatch({
        type: types.SYNC_FIREBASE_CONFIG,
        data: configSnapshot.val(),
      });
    } catch (error) {}
  };
}
