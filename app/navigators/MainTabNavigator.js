import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import { Util } from 'teko-js-sale-library';

import { colors } from '../resources/styles/common';

import HomeContainer from '../containers/HomeContainer';
import { scale } from '../utils/scaling';

// MAIN TABS
export default createBottomTabNavigator(
  {
    Home: HomeContainer,
    Home: HomeContainer,
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: colors.primary,
      inactiveTintColor: colors.gray,
      labelStyle: {
        fontFamily: 'sale-text-regular',
        fontSize: scale(10),
        marginBottom: scale(5),
      },
      style: {
        backgroundColor: 'white',
        height: scale(50),
      },
    },
    navigationOptions: {
      swipeEnabled: true,
      animationEnabled: true,
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        if (navigation.state.routeName === 'Home') {
          Util.Logger.startMeasure('tabBarOnPress_onChooseCategory');
        }
        defaultHandler();
      },
    },
  }
);
