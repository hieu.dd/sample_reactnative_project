import { Dimensions, Platform, StatusBar, StyleSheet } from 'react-native';
import { Constants } from 'expo';
import { scale, getLetterSpacing } from '../../utils/scaling';

const { width, height } = Dimensions.get('window');
const softBarHeight = Dimensions.get('screen').height - Dimensions.get('window').height;
const statusBarHeight = Constants.statusBarHeight;
const headerHeight = Platform.OS === 'ios' ? scale(44) : scale(56);
const base_resolution = Platform.OS === 'ios' ? 375 : 360; // screen width of iphone 6 - Nexus 5X
const real_resolution = width;

export const screen = {
  width,
  height,
  softBarHeight,
  isLandscape: width > height,
  s15: scale(15),
  s20: scale(15),
  padding: {
    tiny: scale(5),
    default: scale(8),
    smaller: scale(12),
    small: scale(16),
    medium: scale(24),
    large: scale(32),
  },
  margin: {
    tiny: scale(5),
    default: scale(8),
    smaller: scale(12),
    small: scale(16),
    medium: scale(24),
    large: scale(32),
  },
  button: {
    height: scale(44),
  },
  iconSize: {
    default: scale(29),
    fw: scale(24),
    small: scale(15),
    medium: scale(20),
    large: scale(35),
  },
  fontSize: {
    title: scale(22),
    tiny: scale(8),
    verySmall: scale(10),
    smaller: scale(10),
    small: scale(12),
    medium: scale(14),
    large: scale(16),
    larger: scale(17),
    veryLarge: scale(22),
  },
  commonRatio: {
    buttonHeight: height * 0.06,
    titleFontSize: height * 0.06,
    smallFontSize: height * 0.02,
    mediumFontSize: height * 0.025,
    fontSize: height * 0.03,
    iconSize: height * 0.04,
    smallIconSize: height * 0.03,
    largeIconSize: height * 0.05,
  },
  header: {
    statusBarHeight: statusBarHeight,
    headerHeight: headerHeight,
    totalHeight: headerHeight + statusBarHeight,
    titleWidth: width - 2 * headerHeight - 80,
    iconSize: Platform.OS === 'ios' ? scale(22) : scale(22),
    smallIconSize: Platform.OS === 'ios' ? scale(15) : scale(15),
    textSize: Platform.OS === 'ios' ? scale(17) : scale(17),
  },
  searchBar: {
    height: Platform.OS === 'ios' ? scale(48) : scale(48),
    fontSize: Platform.OS === 'ios' ? scale(15) : scale(15),
  },
  tabBar: {
    height: Platform.OS === 'ios' ? scale(49) : scale(49),
    labelFontSize: Platform.OS === 'ios' ? scale(11) : scale(13),
    labelIconSize: Platform.OS === 'ios' ? scale(29) : scale(29),
  },
  dimension: {
    emptyCartImageHeight: width / 3,
    imageSizeInSearch: scale(50),
  },
  fontScale: real_resolution / base_resolution,
};

export const colors = {
  lightGold: 'rgb(255,215,83)',
  primary: '#ea341f',
  tomato: '#df2020',
  red: '#CF0000',
  secondary: '#1e75f6',
  black: 'rgb(38,40,41)',
  black60: 'rgba(38,40,41,0.6)',
  black_39: '#393939',
  black_3A: '#3A3A3A',
  black_63: '#636363',
  black_B7: '#B7B7B7',
  black_900: '#212121',
  blue_700: '#1976D2',
  black_44: '#444444',
  black_2C: '#2C2C2C',
  black_24: '#242424',
  backgroundColor: '#ffffff',
  red_DA: '#DA3B42',
  red_CA: '#ca9ca4',
  black_DC: '#DCDCDC',
  gray: '#c6cace',
  darkGray: '#8f9598',
  lightGray: '#eef1f3',
  pumpkinOrange: '#fa7b0f',
  reddishOrange: 'rgb(245,71,30)',
  leafyGreen: 'rgb(84,183,65)',
  tangerine: '#ff9000',
  duckEggBlue: '#eef7fc',
  veryLightPink: 'rgb(255,239,237)',
};

export const gradientColors = {
  primary: [colors.tomato, colors.primary],
  orderStatus: {
    waiting: [colors.pumpkinOrange, 'rgb(241,101,11)'],
    delivering: ['rgb(23,217,242)', 'rgb(45,161,232)'],
    done: ['rgb(163,207,70)', 'rgb(111,191,76)'],
    canceled: [colors.tomato, colors.primary],
  },
};

export const textStyles = StyleSheet.create({
  fontBig: {
    fontFamily: 'sale-text-bold',
    fontSize: scale(24),
    letterSpacing: getLetterSpacing(-0.6),
    lineHeight: scale(32),
    color: colors.black,
  },
  medium: {
    fontFamily: 'sale-text-semibold',
    fontSize: scale(20),
    letterSpacing: getLetterSpacing(-0.8),
    lineHeight: scale(22),
    color: colors.darkGray,
  },
  heading1: {
    fontFamily: 'sale-text-semibold',
    fontSize: scale(15),
    letterSpacing: getLetterSpacing(-0.4),
    lineHeight: scale(20),
    textAlignVertical: 'center',
    color: colors.black,
  },
  heading2: {
    fontFamily: 'sale-text-semibold',
    fontSize: scale(14),
    letterSpacing: getLetterSpacing(-0.2),
    lineHeight: scale(19),
    textAlignVertical: 'center',
    color: colors.black,
  },
  subheading: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(13),
    letterSpacing: getLetterSpacing(-0.1),
    lineHeight: scale(18),
    textAlignVertical: 'center',
    color: colors.darkGray,
  },
  body1: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(15),
    letterSpacing: getLetterSpacing(-0.2),
    lineHeight: scale(20),
    textAlignVertical: 'center',
    color: colors.black,
  },
  body2: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(14),
    letterSpacing: getLetterSpacing(-0.2),
    justifyContent: 'center',
    textAlignVertical: 'center',
    lineHeight: scale(19),
    color: colors.black,
  },
  footnote: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(12),
    lineHeight: scale(16),
    letterSpacing: getLetterSpacing(-0.1),
    textAlignVertical: 'center',
    color: colors.darkGray,
  },
  footnoteRed: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(12),
    lineHeight: scale(16),
    letterSpacing: getLetterSpacing(-0.1),
    textAlignVertical: 'center',
    color: colors.primary,
  },
  display: {
    fontFamily: 'sale-text-semibold',
    fontSize: scale(17),
    letterSpacing: getLetterSpacing(-0.41),
    justifyContent: 'center',
    textAlignVertical: 'center',
    lineHeight: scale(22),
    color: colors.black,
  },
});

const shadow = {
  ...Platform.select({
    ios: {
      shadowOffset: { width: 1, height: 1 },
      shadowColor: 'black',
      shadowOpacity: 0.5,
    },
    android: {
      elevation: 3,
    },
  }),
};
export default StyleSheet.create({
  full: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  fullAndCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  rowAndFull: {
    flex: 1,
    flexDirection: 'row',
  },
  rowAndCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowWithPaddingBottom: {
    flexDirection: 'row',
    paddingBottom: screen.padding.default,
  },
  rowWithTinyPaddingBottom: {
    flexDirection: 'row',
    paddingBottom: screen.padding.tiny,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    marginBottom: 8,
  },
  headerStyle: {
    shadowOpacity: 0,
    shadowOffset: { height: 0 },
    shadowRadius: 0,
    elevation: 0,
    backgroundColor: '#16A085',
    height: Platform.OS === 'ios' ? scale(64) : scale(64),
    paddingTop: statusBarHeight,
    justifyContent: 'center',
  },
  borderButton: {
    backgroundColor: '#005ca4',
    padding: screen.padding.default,
    borderRadius: screen.padding.default,
  },
  shadow,
  card: {
    padding: screen.margin.smaller,
    margin: screen.margin.tiny,
    borderRadius: screen.padding.tiny,
    backgroundColor: 'white',
    ...shadow,
  },
});
