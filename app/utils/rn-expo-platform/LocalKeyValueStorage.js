import { AsyncStorage } from 'react-native';

class LocalKeyValueStorage {
  constructor() {}

  async set(key, stringValue) {
    return AsyncStorage.setItem(key, stringValue);
  }

  async get(key) {
    return AsyncStorage.getItem(key);
  }

  async setJson(key, object) {
    return this.set(key, JSON.stringify(object));
  }

  // this function may throw error, make sure to catch it
  async getJson(key) {
    let data = await this.get(key);
    return JSON.parse(data);
  }
}

export default new LocalKeyValueStorage();
